package id.co.danwinciptaniaga.jessys.web.screens.batch;

import com.haulmont.cuba.gui.screen.*;
import id.co.danwinciptaniaga.jessys.entity.Batch;

@UiController("jessys_Batch.browse")
@UiDescriptor("batch-browse.xml")
@LookupComponent("table")
@LoadDataBeforeShow
public class BatchBrowse extends MasterDetailScreen<Batch> {
}