package id.co.danwinciptaniaga.jessys.rest;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ImageController {
  private static Logger logger = LoggerFactory.getLogger(ImageController.class);

  @GetMapping("/image/logo.png")
  public void getLogo(HttpServletRequest request, HttpServletResponse response) {
    InputStream is = getClass().getResourceAsStream("/jessy_logo.png");
    try {
      logger.debug("Image IS: " + is);
      IOUtils.copy(is, response.getOutputStream());
      response.setStatus(HttpServletResponse.SC_OK);
    } catch (IOException e) {
      logger.error("Failed to get logo", e);
      response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }
  }
}
