package id.co.danwinciptaniaga.jessys;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;
import javax.inject.Inject;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.haulmont.bali.util.ParamsMap;
import com.haulmont.chile.core.model.MetaClass;
import com.haulmont.cuba.core.app.DataService;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.Configuration;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.core.global.View;
import com.haulmont.cuba.gui.AppConfig;
import com.haulmont.cuba.gui.ComponentsHelper;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.WindowManager;
import com.haulmont.cuba.gui.WindowManagerProvider;
import com.haulmont.cuba.gui.backgroundwork.BackgroundWorkWindow;
import com.haulmont.cuba.gui.components.Window;
import com.haulmont.cuba.gui.config.WindowConfig;
import com.haulmont.cuba.gui.config.WindowInfo;
import com.haulmont.cuba.gui.executors.BackgroundTask;
import com.haulmont.cuba.gui.executors.TaskLifeCycle;
import com.haulmont.cuba.gui.export.ByteArrayDataProvider;
import com.haulmont.cuba.gui.export.ExportDisplay;
import com.haulmont.cuba.gui.export.ExportFormat;
import com.haulmont.cuba.gui.screen.FrameOwner;
import com.haulmont.cuba.gui.screen.Screen;
import com.haulmont.cuba.gui.screen.ScreenContext;
import com.haulmont.cuba.gui.screen.UiControllerUtils;
import com.haulmont.cuba.security.entity.User;
import com.haulmont.reports.app.service.ReportService;
import com.haulmont.reports.entity.CubaReportOutputType;
import com.haulmont.reports.entity.Report;
import com.haulmont.reports.entity.ReportGroup;
import com.haulmont.reports.entity.ReportInputParameter;
import com.haulmont.reports.entity.ReportOutputType;
import com.haulmont.reports.entity.ReportTemplate;
import com.haulmont.reports.exception.FailedToConnectToOpenOfficeException;
import com.haulmont.reports.exception.NoOpenOfficeFreePortsException;
import com.haulmont.reports.exception.ReportingException;
import com.haulmont.reports.gui.ReportPrintHelper;
import com.haulmont.reports.gui.ReportSecurityManager;
import com.haulmont.reports.gui.ReportingClientConfig;
import com.haulmont.reports.gui.report.run.InputParametersFrame;
import com.haulmont.reports.gui.report.run.InputParametersWindow;
import com.haulmont.reports.gui.report.run.ShowChartController;
import com.haulmont.reports.gui.report.run.ShowPivotTableController;
import com.haulmont.reports.gui.report.run.ShowReportTable;
import com.haulmont.yarg.reporting.ReportOutputDocument;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Component(CustomReportGuiManager.NAME)
public class CustomReportGuiManager {
  public static final String NAME = "jessys_CustomReportGuiManager";

  @Inject
  protected ReportService reportService;

  @Inject
  protected DataService dataService;

  @Inject
  protected Messages messages;

  @Inject
  protected Metadata metadata;

  @Inject
  protected Configuration configuration;

  @Inject
  protected WindowManagerProvider windowManagerProvider;

  @Inject
  protected WindowConfig windowConfig;

  @Inject
  protected ReportSecurityManager reportSecurityManager;

  @Inject
  protected UserSessionSource userSessionSource;

  public Report getAvailableReportByName(@Nullable String screenId, @Nullable User user,
      @Nullable MetaClass inputValueMetaClass,
      String name) {
    LoadContext<Report> lc = new LoadContext<>(Report.class);
    lc.setLoadDynamicAttributes(true);
    lc.setView(new View(Report.class)
        .addProperty("name")
        .addProperty("localeNames")
        .addProperty("description")
        .addProperty("code")
        .addProperty("group", metadata.getViewRepository().getView(ReportGroup.class, View.LOCAL)));
    lc.setQueryString("select r from report$Report r where r.system <> true and r.name = :name ")
        .setParameter("name", name).setMaxResults(1);
    reportSecurityManager.applySecurityPolicies(lc, screenId, user);
    reportSecurityManager.applyPoliciesByEntityParameters(lc, inputValueMetaClass);
    return dataService.load(lc);
  }

  public void runReportNewTab(Report report, FrameOwner screen) {
    if (report == null) {
      throw new IllegalArgumentException("Can not run null report");
    }
    if (report.getInputParameters() != null && report.getInputParameters().size() > 0
        || inputParametersRequiredByTemplates(report)) {
      openReportParamsNewTab(screen, report, null, null, null);
    } else {
      printReportNewTab(report, ParamsMap.empty(), null, null, null, screen);
    }
  }

  public boolean supportAlterableForTemplate(ReportTemplate template) {
    if (BooleanUtils.isTrue(template.getCustom())) {
      return false;
    }
    if (template.getReportOutputType() == ReportOutputType.CHART
        || template.getReportOutputType() == ReportOutputType.TABLE) {
      return false;
    }
    return BooleanUtils.isTrue(template.getAlterable());
  }

  protected void openReportParamsNewTab(FrameOwner screen, Report report,
      @Nullable Map<String, Object> parameters,
      @Nullable String templateCode, @Nullable String outputFileName) {
    openReportParamsNewTab(screen, report, parameters, null, templateCode, outputFileName, false);
  }

  protected void openReportParamsNewTab(FrameOwner screen, Report report,
      @Nullable Map<String, Object> parameters,
      @Nullable ReportInputParameter inputParameter, @Nullable String templateCode,
      @Nullable String outputFileName,
      boolean bulkPrint) {
    Map<String, Object> params = ParamsMap.of(
        InputParametersFrame.REPORT_PARAMETER, report,
        InputParametersFrame.PARAMETERS_PARAMETER, parameters,
        InputParametersWindow.INPUT_PARAMETER, inputParameter,
        InputParametersWindow.TEMPLATE_CODE_PARAMETER, templateCode,
        InputParametersWindow.OUTPUT_FILE_NAME_PARAMETER, outputFileName,
        InputParametersWindow.BULK_PRINT, bulkPrint
    );

    WindowManager wm = windowManagerProvider.get();

    WindowInfo windowInfo = AppBeans.get(WindowConfig.class).getWindowInfo(
        "report$extInputParameters");

    if (screen != null) {
      ScreenContext screenContext = UiControllerUtils.getScreenContext(screen);
      WindowManager scr = (WindowManager) screenContext.getScreens();
      scr.openWindow(windowInfo, WindowManager.OpenType.NEW_TAB, params);
    } else {
      wm.openWindow(windowInfo, WindowManager.OpenType.NEW_TAB, params);
    }
  }

  public void printReportNewTab(Report report, Map<String, Object> params,
      @Nullable String templateCode,
      @Nullable String outputFileName, @Nullable ReportOutputType outputType, FrameOwner screen) {

    Configuration configuration = AppBeans.get(Configuration.NAME);
    ReportingClientConfig reportingClientConfig = configuration.getConfig(
        ReportingClientConfig.class);

    if (screen != null && reportingClientConfig.getUseBackgroundReportProcessing()) {
      printReportBackground(report, params, templateCode, outputFileName, outputType, screen);
    } else {
      printReportSync(report, params, templateCode, outputFileName, outputType, screen);
    }
  }

  public void printReportSync(Report report, Map<String, Object> params,
      @Nullable String templateCode,
      @Nullable String outputFileName, @Nullable ReportOutputType outputType,
      @Nullable FrameOwner screen) {
    ReportOutputDocument document = getReportResult(report, params, templateCode, outputType);

    showReportResultNewTab(document, params, templateCode, outputFileName, screen);
  }

  protected void showReportResultNewTab(ReportOutputDocument document, Map<String, Object> params,
      @Nullable String templateCode, @Nullable String outputFileName, @Nullable FrameOwner screen) {
    showReportResultNewTab(document, params, templateCode, outputFileName, null, screen);
  }

  protected void showReportResultNewTab(ReportOutputDocument document, Map<String, Object> params,
      @Nullable String templateCode, @Nullable String outputFileName,
      @Nullable ReportOutputType outputType, @Nullable FrameOwner screen) {

    WindowManager wm = windowManagerProvider.get();
    if (document.getReportOutputType().getId().equals(CubaReportOutputType.chart.getId())) {
      Map<String, Object> screenParams = new HashMap<>();
      screenParams.put(ShowChartController.CHART_JSON_PARAMETER,
          new String(document.getContent(), StandardCharsets.UTF_8));
      screenParams.put(ShowChartController.REPORT_PARAMETER, document.getReport());
      screenParams.put(ShowChartController.TEMPLATE_CODE_PARAMETER, templateCode);
      screenParams.put(ShowChartController.PARAMS_PARAMETER, params);

      WindowInfo windowInfo = windowConfig.getWindowInfo("report$showChart");

      if (screen != null) {
        ScreenContext screenContext = UiControllerUtils.getScreenContext(screen);

        WindowManager screens = (WindowManager) screenContext.getScreens();
        screens.openWindow(windowInfo, WindowManager.OpenType.NEW_TAB, screenParams);
      } else {
        wm.openWindow(windowInfo, WindowManager.OpenType.NEW_TAB, screenParams);
      }
    } else if (document.getReportOutputType().getId().equals(CubaReportOutputType.pivot.getId())) {
      Map<String, Object> screenParams = ParamsMap.of(
          ShowPivotTableController.PIVOT_TABLE_DATA_PARAMETER, document.getContent(),
          ShowPivotTableController.REPORT_PARAMETER, document.getReport(),
          ShowPivotTableController.TEMPLATE_CODE_PARAMETER, templateCode,
          ShowPivotTableController.PARAMS_PARAMETER, params);

      WindowInfo windowInfo = windowConfig.getWindowInfo("report$showPivotTable");

      if (screen != null) {
        ScreenContext screenContext = UiControllerUtils.getScreenContext(screen);

        WindowManager screens = (WindowManager) screenContext.getScreens();
        screens.openWindow(windowInfo, WindowManager.OpenType.NEW_TAB, screenParams);
      } else {
        wm.openWindow(windowInfo, WindowManager.OpenType.NEW_TAB, screenParams);
      }
    } else if (document.getReportOutputType().getId().equals(CubaReportOutputType.table.getId())) {
      Map<String, Object> screenParams = new HashMap<>();
      screenParams.put(ShowReportTable.TABLE_DATA_PARAMETER, document.getContent());
      screenParams.put(ShowReportTable.REPORT_PARAMETER, document.getReport());
      screenParams.put(ShowReportTable.TEMPLATE_CODE_PARAMETER, templateCode);
      screenParams.put(ShowReportTable.PARAMS_PARAMETER, params);

      WindowInfo windowInfo = windowConfig.getWindowInfo("report$showReportTable");

      if (screen != null) {
        ScreenContext screenContext = UiControllerUtils.getScreenContext(screen);

        WindowManager screens = (WindowManager) screenContext.getScreens();
        screens.openWindow(windowInfo, WindowManager.OpenType.NEW_TAB, screenParams);
      } else {
        wm.openWindow(windowInfo, WindowManager.OpenType.NEW_TAB, screenParams);
      }
    } else {
      ExportDisplay exportDisplay;
      byte[] byteArr = document.getContent();
      com.haulmont.yarg.structure.ReportOutputType finalOutputType =
          (outputType != null) ? outputType.getOutputType() : document.getReportOutputType();

      ExportFormat exportFormat = ReportPrintHelper.getExportFormat(finalOutputType);

      if (screen != null) {
        Screen hostScreen = UiControllerUtils.getScreen(screen);
        exportDisplay = AppConfig.createExportDisplay(hostScreen.getWindow());
      } else {
        exportDisplay = AppConfig.createExportDisplay(null);
      }
      String documentName = isNotBlank(outputFileName) ?
          outputFileName :
          document.getDocumentName();
      exportDisplay.show(new ByteArrayDataProvider(byteArr), documentName, exportFormat);
    }
  }

  public void printReportBackground(Report report, final Map<String, Object> params,
      @Nullable String templateCode,
      @Nullable String outputFileName, @Nullable ReportOutputType outputType, FrameOwner screen) {
    ReportingClientConfig reportingClientConfig = configuration.getConfig(
        ReportingClientConfig.class);

    Report targetReport = getReportForPrinting(report);

    long timeout = reportingClientConfig.getBackgroundReportProcessingTimeoutMs();
    UUID userSessionId = userSessionSource.getUserSession().getId();

    Screen hostScreen = UiControllerUtils.getScreen(screen);

    BackgroundTask<Void, ReportOutputDocument> task =
        new BackgroundTask<Void, ReportOutputDocument>(timeout, TimeUnit.MILLISECONDS, hostScreen) {

          @SuppressWarnings("UnnecessaryLocalVariable")
          @Override
          public ReportOutputDocument run(TaskLifeCycle<Void> taskLifeCycle) {
            ReportOutputDocument result = getReportResult(targetReport, params, templateCode,
                outputType);
            return result;
          }

          @Override
          public boolean handleException(Exception ex) {
            if (ex instanceof ReportingException) {
              if (ex instanceof FailedToConnectToOpenOfficeException) {
                String caption = messages.getMessage(getClass(),
                    "reportException.failedConnectToOffice");
                return showErrorNotification(caption);
              } else if (ex instanceof NoOpenOfficeFreePortsException) {
                String caption = messages.getMessage(getClass(),
                    "reportException.noOpenOfficeFreePorts");
                return showErrorNotification(caption);
              }
            }
            return super.handleException(ex);
          }

          protected boolean showErrorNotification(String text) {
            Screen ownerScreen = this.getOwnerScreen();
            if (ownerScreen != null) {
              Window window = ownerScreen.getWindow();
              if (window != null) {
                ScreenContext screenContext = ComponentsHelper.getScreenContext(window);
                if (screenContext != null) {
                  Notifications notifications = screenContext.getNotifications();
                  notifications.create()
                      .withCaption(text)
                      .withType(Notifications.NotificationType.ERROR)
                      .show();
                  return true;
                }
              }
            }
            return false;
          }

          @Override
          public void done(ReportOutputDocument document) {
            showReportResultNewTab(document, params, templateCode, outputFileName, outputType,
                screen);
          }

          @Override
          public void canceled() {
            super.canceled();
            reportService.cancelReportExecution(userSessionId, report.getId());
          }
        };

    String caption = messages.getMessage(getClass(), "runReportBackgroundTitle");
    String description = messages.getMessage(getClass(), "runReportBackgroundMessage");

    BackgroundWorkWindow.show(task, caption, description, true);
  }

  public boolean inputParametersRequiredByTemplates(Report report) {
    return report.getTemplates() != null && report.getTemplates().size() > 1
        || containsAlterableTemplate(report);
  }

  public boolean containsAlterableTemplate(Report report) {
    if (report.getTemplates() == null)
      return false;
    for (ReportTemplate template : report.getTemplates()) {
      if (supportAlterableForTemplate(template)) {
        return true;
      }
    }
    return false;
  }

  protected Report getReportForPrinting(Report report) {
    Report copy = metadata.getTools().copy(report);
    copy.setIsTmp(report.getIsTmp());
    return copy;
  }

  public ReportOutputDocument getReportResult(Report report, Map<String, Object> params,
      @Nullable String templateCode, @Nullable ReportOutputType outputType) {
    ReportOutputDocument document;
    if (StringUtils.isBlank(templateCode) && outputType == null) {
      document = reportService.createReport(report, params);
    } else if (!StringUtils.isBlank(templateCode) && outputType == null) {
      document = reportService.createReport(report, templateCode, params);
    } else if (!StringUtils.isBlank(templateCode) && outputType != null) {
      document = reportService.createReport(report, templateCode, params, outputType);
    } else {
      document = reportService.createReport(report, params, outputType);
    }
    return document;
  }

  public void bulkPrint(Report report, @Nullable String templateCode,
      @Nullable ReportOutputType outputType, String alias,
      Collection selectedEntities, @Nullable FrameOwner screen,
      @Nullable Map<String, Object> additionalParameters) {
    ReportingClientConfig reportingClientConfig = configuration.getConfig(
        ReportingClientConfig.class);
    if (screen != null && reportingClientConfig.getUseBackgroundReportProcessing()) {
      bulkPrintBackground(report, templateCode, outputType, alias, selectedEntities, screen,
          additionalParameters);
    } else {
      bulkPrintSync(report, templateCode, outputType, alias, selectedEntities, screen,
          additionalParameters);
    }
  }

  public void bulkPrintBackground(Report report, @Nullable String templateCode,
      @Nullable ReportOutputType outputType,
      String alias, Collection selectedEntities, FrameOwner screen,
      Map<String, Object> additionalParameters) {
    ReportingClientConfig reportingClientConfig = configuration.getConfig(
        ReportingClientConfig.class);

    Report targetReport = getReportForPrinting(report);

    long timeout = reportingClientConfig.getBackgroundReportProcessingTimeoutMs();

    List<Map<String, Object>> paramsList = new ArrayList<>();
    for (Object selectedEntity : selectedEntities) {
      Map<String, Object> map = new HashMap<>();
      map.put(alias, selectedEntity);
      if (additionalParameters != null) {
        map.putAll(additionalParameters);
      }
      paramsList.add(map);
    }

    Screen hostScreen = UiControllerUtils.getScreen(screen);

    BackgroundTask<Void, ReportOutputDocument> task =
        new BackgroundTask<Void, ReportOutputDocument>(timeout, TimeUnit.MILLISECONDS, hostScreen) {
          @SuppressWarnings("UnnecessaryLocalVariable")
          @Override
          public ReportOutputDocument run(TaskLifeCycle<Void> taskLifeCycle) {
            ReportOutputDocument result = reportService.bulkPrint(targetReport, templateCode,
                outputType, paramsList);
            return result;
          }

          @Override
          public void done(ReportOutputDocument result) {
            ExportDisplay exportDisplay = AppConfig.createExportDisplay(hostScreen.getWindow());
            String documentName = result.getDocumentName();
            exportDisplay.show(new ByteArrayDataProvider(result.getContent()), documentName,
                ExportFormat.ZIP);
          }
        };

    String caption = messages.getMessage(getClass(), "runReportBackgroundTitle");
    String description = messages.getMessage(getClass(), "runReportBackgroundMessage");

    BackgroundWorkWindow.show(task, caption, description, true);
  }

  public void bulkPrintSync(Report report, @Nullable String templateCode,
      @Nullable ReportOutputType outputType,
      String alias, Collection selectedEntities, @Nullable FrameOwner screen,
      Map<String, Object> additionalParameters) {
    List<Map<String, Object>> paramsList = new ArrayList<>();
    for (Object selectedEntity : selectedEntities) {
      Map<String, Object> map = new HashMap<>();
      map.put(alias, selectedEntity);
      if (additionalParameters != null) {
        map.putAll(additionalParameters);
      }
      paramsList.add(map);
    }

    Screen hostScreen = UiControllerUtils.getScreen(screen);

    ReportOutputDocument reportOutputDocument = reportService.bulkPrint(report, templateCode,
        outputType, paramsList);
    ExportDisplay exportDisplay = AppConfig.createExportDisplay(hostScreen.getWindow());
    String documentName = reportOutputDocument.getDocumentName();
    exportDisplay.show(new ByteArrayDataProvider(reportOutputDocument.getContent()), documentName,
        ExportFormat.ZIP);
  }
}