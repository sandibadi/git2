package id.co.danwinciptaniaga.jessys.web.screens.order;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;

import id.co.danwinciptaniaga.jessys.entity.OrderLine;
import id.co.danwinciptaniaga.jessys.entity.Product;
import id.co.danwinciptaniaga.jessys.service.OrderService;

@UiController("jessys_OrderLine.edit")
@UiDescriptor("order-line-edit.xml")
@EditedEntityContainer("orderLineDc")
@LoadDataBeforeShow
public class OrderLineEdit extends StandardEditor<OrderLine> {
  @Inject
  Logger logger;

  @Inject
  private DataManager dataManager;
  @Inject
  private DataContext dataContext;
  @Inject
  private OrderService orderService;
  @Inject
  private InstanceContainer<OrderLine> orderLineDc;

  @Subscribe("productField")
  public void onProductFieldValueChange(HasValue.ValueChangeEvent<Product> event) {
    // kalau User pilih Product, update price
    Product p = dataManager.load(Product.class).view("_local").id(event.getValue().getId()).one();
    getEditedEntity().setUnitPrice(p.getSellingPrice());
  }

  @Subscribe
  public void onBeforeCommitChanges(BeforeCommitChangesEvent event) {
    final OrderLine ol = getEditedEntity();
    logger.debug("BeforeCommitChanges called: {} x {} = {}", ol.getQuantity(), ol.getUnitPrice(),
        ol.getLineTotal());
    BigDecimal linetotal = orderService.calculateOrderLineTotal(ol);
    ol.setLineTotal(linetotal);
    logger.debug("BeforeCommitChanges called after Service: {} x {} = {}", ol.getQuantity(),
        ol.getUnitPrice(), ol.getLineTotal());
  }

}