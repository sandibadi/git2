package id.co.danwinciptaniaga.jessys.web.screens.customer;

import javax.inject.Inject;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.gui.screen.*;

import id.co.danwinciptaniaga.jessys.entity.Customer;
import id.co.danwinciptaniaga.jessys.service.CustomerService;

@UiController("jessys_Customer.browse")
@UiDescriptor("customer-browse.xml")
@LookupComponent("table")
@LoadDataBeforeShow
public class CustomerBrowse extends MasterDetailScreen<Customer> {
  @Inject
  private CustomerService customerService;

  @Subscribe
  public void onInitEntity(InitEntityEvent<Customer> event) {
    Customer c = event.getEntity();

    c.setCode(customerService.getNextCustCode());
  }

}