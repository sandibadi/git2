package id.co.danwinciptaniaga.jessys.web.screens.order;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

import javax.inject.Inject;

import org.slf4j.Logger;

import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.core.global.EntityStates;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.core.global.MetadataTools;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.ScreenBuilders;
import com.haulmont.cuba.gui.components.Action;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.DateField;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.components.LookupPickerField;
import com.haulmont.cuba.gui.components.RadioButtonGroup;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.components.TextField;
import com.haulmont.cuba.gui.components.actions.BaseAction;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.model.InstanceLoader;
import com.haulmont.cuba.gui.screen.EditedEntityContainer;
import com.haulmont.cuba.gui.screen.MessageBundle;
import com.haulmont.cuba.gui.screen.OpenMode;
import com.haulmont.cuba.gui.screen.Screen;
import com.haulmont.cuba.gui.screen.StandardEditor;
import com.haulmont.cuba.gui.screen.StandardOutcome;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.Target;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;

import id.co.danwinciptaniaga.jessys.OrderHelper;
import id.co.danwinciptaniaga.jessys.UIHelper;
import id.co.danwinciptaniaga.jessys.entity.Batch;
import id.co.danwinciptaniaga.jessys.entity.Customer;
import id.co.danwinciptaniaga.jessys.entity.ExistingOrNew;
import id.co.danwinciptaniaga.jessys.entity.Order;
import id.co.danwinciptaniaga.jessys.entity.OrderLine;
import id.co.danwinciptaniaga.jessys.service.CustomerService;
import id.co.danwinciptaniaga.jessys.service.OrderService;

@UiController("jessys_Order.edit")
@UiDescriptor("order-edit.xml")
@EditedEntityContainer("orderDc")
public class OrderEdit extends StandardEditor<Order> {
  @Inject
  Logger logger;

  @Inject
  private ScreenBuilders screenBuilders;
  @Inject
  private Metadata metadata;
  @Inject
  protected Notifications notifications;
  @Inject
  private MessageBundle messageBundle;
  @Inject
  private EntityStates entityStates;
  @Inject
  private MetadataTools metadataTools;

  @Inject
  private OrderService orderService;
  @Inject
  private OrderHelper orderHelper;
  @Inject
  private CustomerService customerService;

  @Inject
  private DataContext dataContext;
  @Inject
  private InstanceContainer<Order> orderDc;
  @Inject
  private InstanceLoader<Order> orderDl;
  @Inject
  private CollectionLoader<OrderLine> orderLinesDl;
  @Inject
  private CollectionLoader<Batch> batchDl;
  @Inject
  private Table<OrderLine> orderLinesTable;
  @Inject
  private CollectionLoader<Customer> customerDl;

  @Inject
  private TextField<String> salesRepField;
  @Inject
  private TextField<String> phoneField;
  @Inject
  private TextField<String> orderNoField;
  @Inject
  private TextField<String> descriptionField;
  @Inject
  private TextField<BigDecimal> deliveryFeeField;
  @Inject
  private TextField<String> customerNameField;
  @Inject
  private LookupField<Batch> batchField;
  @Inject
  private TextField<String> addressField;
  @Inject
  private DateField<Date> orderDateField;
  @Inject
  private Button bookButton;
  @Inject
  private Button unbookButton;
  @Inject
  private RadioButtonGroup<ExistingOrNew> customerNewOrExisting;
  @Inject
  private LookupPickerField<Customer> customerField;

  @Subscribe
  public void onInit(InitEvent event) {
    Action bookOrderAction = new BaseAction("bookOrder")
        .withCaption(messageBundle.getMessage("btnBook"))
        .withPrimary(true)
        .withHandler(this::onBookButtonClick);

    bookButton.setAction(bookOrderAction);

    Action unbookOrderAction = new BaseAction("unbookOrder")
        .withCaption(messageBundle.getMessage("btnUnbook"))
        .withPrimary(true)
        .withHandler(this::onUnbookButtonClick);

    unbookButton.setAction(unbookOrderAction);
  }

  @Subscribe
  public void onInitEntity(InitEntityEvent<Order> event) {
    Order order = event.getEntity();
    if (entityStates.isNew(order)) {
      logger.debug("initializing new order");
      // buat baru
      Date now = Calendar.getInstance().getTime();
      order.setSubtotal(BigDecimal.ZERO);
      order.setGrandtotal(BigDecimal.ZERO);
      order.setDeliveryFee(BigDecimal.ZERO);
      order.setOrderDate(now);
      Batch batch = orderHelper.getBatch(now);
      logger.debug("Found current batch: {}", batch);
      order.setBatch(batch);
      customerNewOrExisting.setValue(ExistingOrNew.NEW);
    } else {
      logger.debug("no need to initialize order");
      // edit
    }
  }

  @Subscribe
  public void onBeforeShow(BeforeShowEvent event) {
    orderDl.load();
    customerDl.load();
    orderLinesDl.setParameter("order", orderDl.getContainer().getItem());
    orderLinesDl.load();
    batchDl.load();
    if (!isReadOnly() && !entityStates.isNew(getEditedEntity())) {
      if (getEditedEntity().getBooked()) {
        batchField.setEditable(false);
        orderNoField.setEditable(false);
        descriptionField.setEditable(false);
        customerNewOrExisting.setEditable(false);
        if (customerField.getValue() != null) {
          customerNewOrExisting.setValue(ExistingOrNew.EXISTING);
          customerField.setVisible(true);
        } else {
          customerNewOrExisting.setValue(ExistingOrNew.NEW);
          customerField.setVisible(false);
        }
        customerField.setEditable(false);
        customerNameField.setEditable(false);
        addressField.setEditable(false);
        phoneField.setEditable(false);
        salesRepField.setEditable(false);
        deliveryFeeField.setEditable(false);
        orderDateField.setEditable(false);
        logger.debug("Disabling actions");
        orderLinesTable.getActions().stream().forEach(e -> e.setEnabled(false));

        bookButton.setVisible(false);
        unbookButton.setVisible(true);
        unbookButton.setEnabled(true);
      } else {
        // check existing or new customer
        setExistingOrNewCustomer(true);

        bookButton.setVisible(true);
        bookButton.setEnabled(true);
        unbookButton.setVisible(false);
      }

    } else {
      bookButton.setVisible(false);
      unbookButton.setVisible(false);
    }
  }

  @Subscribe(target = Target.DATA_CONTEXT)
  public void onChange(DataContext.ChangeEvent event) {
    Entity o = event.getEntity();
    logger.debug("------DataContext.ChangeEvent: {} - {}", o, event);
    if (o != null && Order.class.isAssignableFrom(o.getClass())) {
      if (!entityStates.isNew(o)) {
        logger.debug("Enabling actions");
        orderLinesTable.getActions().stream().forEach(e -> e.setEnabled(true));
      } else {
        logger.debug("Disabling actions");
        orderLinesTable.getActions().stream().forEach(e -> e.setEnabled(false));
      }
    }
  }

  @Subscribe(id = "orderLinesDc", target = Target.DATA_CONTAINER)
  public void onOrderLinesDcCollectionChange(
      CollectionContainer.CollectionChangeEvent<OrderLine> event) {
    updateOrderDueToLine();
  }

  @Subscribe(id = "orderLinesDc", target = Target.DATA_CONTAINER)
  public void onOrderLinesDcItemPropertyChange(
      InstanceContainer.ItemPropertyChangeEvent<OrderLine> event) {
    updateOrderDueToLine();
  }

  private void updateOrderDueToLine() {
    BigDecimal subTotal = orderService.calculateOrderLinesTotal(
        orderLinesTable.getItems().getItems());
    getEditedEntity().setSubtotal(subTotal);
    BigDecimal grandTotal = orderService.calculateOrderTotal(getEditedEntity(),
        orderLinesTable.getItems().getItems());
    if (getEditedEntity().getGrandtotal() != null
        && getEditedEntity().getGrandtotal().compareTo(grandTotal) != 0) {
      // hanya update grandTotal kalau beda
      getEditedEntity().setGrandtotal(grandTotal);
    }
  }

  @Subscribe(target = Target.DATA_CONTEXT)
  public void onPostCommit(DataContext.PostCommitEvent event) {
    logger.debug("Enabling actions");
    orderLinesTable.getActions().stream().forEach(e -> e.setEnabled(true));
  }

  @Subscribe("orderLinesTable.create")
  public void onOrderLinesTableCreate(Action.ActionPerformedEvent event) {
    if (orderDc.getItem() != null) {
      OrderLine newOrderLine = metadata.create(OrderLine.class);
      newOrderLine.setOrder(orderDc.getItem());
      int nextNo = getNextNo(orderLinesTable.getItems().getItems());
      newOrderLine.setLine(nextNo);

      Screen screen = screenBuilders.editor(orderLinesTable)
          .newEntity(newOrderLine)
          .withLaunchMode(OpenMode.NEW_WINDOW)
          .withParentDataContext(dataContext)
          .build();
      screen.addAfterCloseListener(afterCloseEvent -> {
        logger.debug("afterClose OrderLineEditor: {}", afterCloseEvent);
      });
      screen.show();
    } else {
      UIHelper.showNotification(notifications,
          messageBundle.getMessage("notification.noOrderSelected"),
          Notifications.NotificationType.HUMANIZED);
    }
  }

  @Subscribe("deliveryFeeField")
  public void onDeliveryFeeFieldValueChange(HasValue.ValueChangeEvent<BigDecimal> event) {
    // langsung update layar
    updateOrderDueToLine();
  }

  @Subscribe
  public void onBeforeCommitChanges(BeforeCommitChangesEvent event) {
    // memastikan hasil akhir selalu benar
    updateOrderDueToLine();
    if (ExistingOrNew.NEW.equals(customerNewOrExisting.getValue())) {
      // buat customer baru
      Customer c = event.getDataContext().create(Customer.class);
      c.setCode(customerService.getNextCustCode());
      c.setName(customerNameField.getValue());
      c.setAddress(addressField.getValue());
      c.setPhone(phoneField.getValue());
      getEditedEntity().setCustomer(c);
    } else {
      logger.debug("Saving existing customer, updating Order");
      Order o = getEditedEntity();
      Customer eCust = o.getCustomer();
      if (eCust != null) {
        o.setCustomerName(eCust.getName());
        o.setAddress(eCust.getAddress());
        o.setPhone(eCust.getPhone());
      }
    }
  }

  @Subscribe
  public void onAfterCommitChanges(AfterCommitChangesEvent event) {
    Order o = getEditedEntity();
    if (o.getCustomer() != null) {
      if (ExistingOrNew.NEW.equals(customerNewOrExisting.getValue())) {
        String z = metadataTools.getInstanceName(o.getCustomer());
        UIHelper.showNotification(notifications, String.format("New Customer created: %s", z),
            Notifications.NotificationType.HUMANIZED);
        // kalau tadinya New, setelah commit, ubah jadi existing
        customerField.setValue(o.getCustomer());
        setExistingOrNewCustomer(true);
      }
    }
  }

  @Subscribe("customerField")
  public void onCustomerFieldValueChange(HasValue.ValueChangeEvent<Customer> event) {
    setExistingOrNewCustomer(false);
  }

  private int getNextNo(Collection<OrderLine> items) {
    // get next number
    int nextNo = 1;
    Collection<OrderLine> ols = items;
    if (ols != null) {
      for (OrderLine z : ols) {
        if (z.getLine() != null && z.getLine() >= nextNo) {
          nextNo = z.getLine() + 1;
        }
      }
    }
    return nextNo;
  }

  @Subscribe("orderLinesTable.edit")
  public void onOrderLinesTableEdit(Action.ActionPerformedEvent event) {
    screenBuilders.editor(orderLinesTable)
        .withLaunchMode(OpenMode.NEW_TAB)
        .withParentDataContext(dataContext)
        .withInitializer(orderLine -> {
          orderLine.setLine(getNextNo(orderLinesTable.getItems().getItems()));
        })
        .build()
        .show();
  }

  private void onBookButtonClick(Action.ActionPerformedEvent actionPerformedEvent) {
    // simpan perubahan di layar
    commitChanges();

    // book Order
    try {
      orderService.bookOrder(getEditedEntity());
      UIHelper.showNotification(notifications,
          String.format(messageBundle.getMessage("notification.order.bookedSuccess"),
              metadataTools.getInstanceName(getEditedEntity())),
          Notifications.NotificationType.HUMANIZED);
      close(StandardOutcome.COMMIT);
    } catch (Exception e) {
      UIHelper.showNotification(notifications,
          String.format(messageBundle.getMessage("notification.order.bookFail"),
              metadataTools.getInstanceName(getEditedEntity()),
              e.getMessage()),
          Notifications.NotificationType.ERROR);
    }
  }

  private void onUnbookButtonClick(Action.ActionPerformedEvent actionPerformedEvent) {
    // unbook Order
    try {
      orderService.unbookOrder(getEditedEntity());
      UIHelper.showNotification(notifications,
          String.format(messageBundle.getMessage("notification.order.unbookSuccess"),
              metadataTools.getInstanceName(getEditedEntity())),
          Notifications.NotificationType.HUMANIZED);
      close(StandardOutcome.COMMIT);
    } catch (Exception e) {
      UIHelper.showNotification(notifications,
          String.format(messageBundle.getMessage("notification.order.unbookFail"),
              metadataTools.getInstanceName(getEditedEntity()),
              e.getMessage()),
          Notifications.NotificationType.ERROR);
    }
  }

  @Subscribe("customerNewOrExisting")
  public void onCustomerNewOrExistingValueChange(HasValue.ValueChangeEvent<ExistingOrNew> event) {
    if (event.getValue().equals(ExistingOrNew.NEW)) {
      getEditedEntity().setCustomer(null);
      setExistingOrNewCustomer(false);
    } else {
      customerField.setVisible(true);
    }
  }

  private void setExistingOrNewCustomer(boolean changeRadioButton) {
    if (customerField.getValue() != null) {
      Customer eCust = customerField.getValue();
      if (changeRadioButton) {
        customerNewOrExisting.setValue(ExistingOrNew.EXISTING);
      }
      customerField.setVisible(true);

      if (!Objects.equals(customerNameField.getValue(), eCust.getName())) {
        customerNameField.setValue(eCust.getName());
      }
      customerNameField.setEditable(false);
      if (!Objects.equals(addressField.getValue(), eCust.getAddress())) {
        addressField.setValue(eCust.getAddress());
      }
      addressField.setEditable(false);
      if (!Objects.equals(phoneField.getValue(), eCust.getPhone())) {
        phoneField.setValue(eCust.getPhone());
      }
      phoneField.setEditable(false);
    } else {
      if (changeRadioButton) {
        customerNewOrExisting.setValue(ExistingOrNew.NEW);
      }
      customerField.setVisible(false);

      customerNameField.setEditable(true);
      addressField.setEditable(true);
      phoneField.setEditable(true);
    }
  }
}