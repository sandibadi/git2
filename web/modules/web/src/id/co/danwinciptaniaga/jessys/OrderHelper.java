package id.co.danwinciptaniaga.jessys;

import java.util.Date;

import javax.inject.Inject;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Component;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.gui.Notifications;

import id.co.danwinciptaniaga.jessys.entity.Batch;

@Component(OrderHelper.NAME)
public class OrderHelper {
  public static final String NAME = "jessys_OrderHelper";

  @Inject
  private DataManager dataManager;

  public Batch getBatch(Date date) {
    Date datePlusOne = DateUtils.addDays(date, 1);
    LoadContext<Batch> batchLc = LoadContext.create(Batch.class);
    StringBuilder query = new StringBuilder();
    query.append("select e from jessys_Batch e ")
        .append("where e.active = true and e.startDate <= :date ")
        .append("and (e.endDate is null or e.endDate > :date) ")
        .append("order by e.startDate desc, e.batchNo ");
    batchLc.setView("_local")
        .setQueryString(query.toString())
        .setParameter("date", datePlusOne)
        .setMaxResults(1);
    Batch batch = dataManager.load(batchLc);
    return batch;
  }
}
