package id.co.danwinciptaniaga.jessys.web.screens.customreport;

import java.util.Map;

import javax.inject.Inject;

import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;
import com.haulmont.reports.entity.Report;
import com.haulmont.reports.entity.ReportTemplate;
import com.haulmont.reports.gui.report.run.InputParametersWindow;

import id.co.danwinciptaniaga.jessys.CustomReportGuiManager;

@UiController("report$extInputParameters")
@UiDescriptor("/com/haulmont/reports/gui/report/run/input-parameters.xml")
public class ExtInputParametersWindow extends InputParametersWindow {

  @Inject
  private CustomReportGuiManager customReportGuiManager;

  public void printReport() {
    if (inputParametersFrame.getReport() != null) {
      if (validateAll()) {
        ReportTemplate template = inputParametersFrame.getReportTemplate();
        if (template != null) {
          templateCode = template.getCode();
        }
        Report report = inputParametersFrame.getReport();
        Map<String, Object> parameters = inputParametersFrame.collectParameters();
        if (bulkPrint) {
          customReportGuiManager.bulkPrint(report, templateCode,
              inputParametersFrame.getOutputType(),
              inputParameter.getAlias(), selectedEntities, this, parameters);
        } else {
          customReportGuiManager.printReportNewTab(report, parameters, templateCode, outputFileName,
              inputParametersFrame.getOutputType(), this);
        }
      }
    }
  }
}