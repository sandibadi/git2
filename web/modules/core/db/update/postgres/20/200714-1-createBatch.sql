create table JESSYS_BATCH (
    ID uuid,
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    --
    BATCH_NO integer not null,
    START_DATE date,
    END_DATE date,
    ACTIVE boolean not null,
    --
    primary key (ID)
);