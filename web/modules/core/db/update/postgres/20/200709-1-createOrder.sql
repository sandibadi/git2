create table JESSYS_ORDER (
    ID uuid,
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    --
    ORDER_NO varchar(30) not null,
    CUSTOMER_NAME varchar(255) not null,
    ADDRESS varchar(255),
    PHONE varchar(15),
    SALES_REP varchar(255),
    DELIVERY_FEE decimal(19) not null,
    SUBTOTAL decimal(19) not null,
    GRANDTOTAL decimal(19) not null,
    ORDER_DATE date not null,
    INVOICE_DATE date,
    INVOICED boolean default false,
    BOOKED boolean default false,
    --
    primary key (ID)
);