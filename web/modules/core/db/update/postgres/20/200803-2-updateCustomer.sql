alter table JESSYS_CUSTOMER add column ACTIVE boolean ^
update JESSYS_CUSTOMER set ACTIVE = false where ACTIVE is null ;
alter table JESSYS_CUSTOMER alter column ACTIVE set not null ;
