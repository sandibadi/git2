alter table JESSYS_ORDER add constraint FK_JESSYS_ORDER_ON_CUSTOMER foreign key (CUSTOMER_ID) references JESSYS_CUSTOMER(ID);
create index IDX_JESSYS_ORDER_ON_CUSTOMER on JESSYS_ORDER (CUSTOMER_ID);
