-- begin JESSYS_PRODUCT
create table JESSYS_PRODUCT (
    ID uuid,
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    --
    CODE varchar(20) not null,
    NAME varchar(255) not null,
    SUPPLIER_ID uuid not null,
    PURCHASE_PRICE decimal(19) not null,
    SELLING_PRICE decimal(19),
    TENANT_ID varchar(255),
    --
    primary key (ID)
)^
-- end JESSYS_PRODUCT
-- begin JESSYS_SUPPLIER
create table JESSYS_SUPPLIER (
    ID uuid,
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    --
    NAME varchar(255) not null,
    ACTIVE boolean not null default true,
    TENANT_ID varchar(255),
    --
    primary key (ID)
)^
-- end JESSYS_SUPPLIER
-- begin JESSYS_ORDER
create table JESSYS_ORDER (
    ID uuid,
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    --
    BATCH_ID uuid not null,
    ORDER_NO varchar(30),
    CUSTOMER_ID uuid,
    CUSTOMER_NAME varchar(255) not null,
    DESCRIPTION varchar(2000),
    ADDRESS varchar(255),
    PHONE varchar(15),
    SALES_REP varchar(255),
    DELIVERY_FEE decimal(19) not null,
    SUBTOTAL decimal(19) not null,
    GRANDTOTAL decimal(19) not null,
    ORDER_DATE date not null,
    INVOICE_DATE date,
    INVOICED boolean default false,
    BOOKED boolean default false,
    FULLY_PAID boolean,
    TENANT_ID varchar(255),
    --
    primary key (ID)
)^
-- end JESSYS_ORDER
-- begin JESSYS_ORDER_LINE
create table JESSYS_ORDER_LINE (
    ID uuid,
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    --
    LINE integer not null,
    ORDER_ID uuid not null,
    PRODUCT_ID uuid not null,
    PRICE decimal(19) not null,
    QUANTITY integer not null,
    LINE_TOTAL decimal(19) not null,
    TENANT_ID varchar(255),
    --
    primary key (ID)
)^
-- end JESSYS_ORDER_LINE
-- begin JESSYS_BATCH
create table JESSYS_BATCH (
    ID uuid,
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    --
    BATCH_NO integer not null,
    START_DATE date,
    END_DATE date,
    ACTIVE boolean not null,
    TENANT_ID varchar(255),
    --
    primary key (ID)
)^
-- end JESSYS_BATCH
-- begin JESSYS_CUSTOMER
create table JESSYS_CUSTOMER (
    ID uuid,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    --
    CODE bigint not null,
    NAME varchar(255),
    ADDRESS varchar(255) not null,
    PHONE varchar(15),
    ACTIVE boolean not null,
    TENANT_ID varchar(255),
    --
    primary key (ID)
)^
-- end JESSYS_CUSTOMER
