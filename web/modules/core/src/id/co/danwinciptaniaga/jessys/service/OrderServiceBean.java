package id.co.danwinciptaniaga.jessys.service;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.haulmont.addon.sdbmt.core.app.multitenancy.TenantProvider;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.TransactionalDataManager;
import com.haulmont.cuba.core.app.LocalizedMessageService;
import com.haulmont.cuba.core.app.UniqueNumbersAPI;
import com.haulmont.cuba.core.entity.KeyValueEntity;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.core.global.ValueLoadContext;

import id.co.danwinciptaniaga.jessys.AppException;
import id.co.danwinciptaniaga.jessys.JasperUtil;
import id.co.danwinciptaniaga.jessys.entity.Order;
import id.co.danwinciptaniaga.jessys.entity.OrderLine;

@Service(OrderService.NAME)
public class OrderServiceBean implements OrderService {
  private static Logger logger = LoggerFactory.getLogger(OrderService.class);
  public static SimpleDateFormat SDF_YYMM = new SimpleDateFormat("yyMM");
  public static SimpleDateFormat SDF_YY = new SimpleDateFormat("yy");

  @Inject
  private Metadata metadata;
  @Inject
  private Persistence persistence;
  @Inject
  private TransactionalDataManager transactionalDataManager;
  @Inject
  private UniqueNumbersAPI uniqueNumbersAPI;
  @Inject
  private LocalizedMessageService localizedMessageService;
  @Inject
  private TenantProvider tenantProvider;

  @Override
  public BigDecimal calculateOrderLineTotal(OrderLine orderLine) {
    int qty = orderLine.getQuantity();
    BigDecimal price = orderLine.getUnitPrice();
    BigDecimal lineTotal = price.multiply(new BigDecimal(qty));

    return lineTotal;
  }

  @Override
  public BigDecimal calculateOrderLinesTotal(Collection<OrderLine> orderLines) {
    BigDecimal linesTotal = BigDecimal.ZERO;
    if (CollectionUtils.isNotEmpty(orderLines)) {
      for (OrderLine ol : orderLines) {
        linesTotal = linesTotal.add(calculateOrderLineTotal(ol));
      }
    }
    return linesTotal;
  }

  @Override
  public BigDecimal calculateOrderTotal(Order order, Collection<OrderLine> orderLines) {
    BigDecimal linesTotal = calculateOrderLinesTotal(orderLines);
    BigDecimal deliveryFee = order.getDeliveryFee();
    BigDecimal grandTotal = deliveryFee.add(linesTotal);

    return grandTotal;
  }

  @Override
  public void bookOrder(Order order) {
    logger.debug("START: Book Order {}", order);
    if (Boolean.valueOf(order.getBooked())) {
      throw new IllegalStateException("Order sudah di-Book");
    }

    ValueLoadContext.Query ols = ValueLoadContext.createQuery(
        "select count(e) from jessys_OrderLine e where e.order = :order");
    ols.setParameter("order", order);

    ValueLoadContext vlc = ValueLoadContext.create()
        .setQuery(ols)
        .addProperty("count");
    List<KeyValueEntity> kves = transactionalDataManager.loadValues(vlc);
    long count = 0;
    if (CollectionUtils.isNotEmpty(kves)) {
      Optional<KeyValueEntity> kve = kves.stream().findFirst();
      if (kve.isPresent()) {
        count = kve.get().getValue("count");
      }
    }
    if (count == 0) {
      // @todo cari cara throw localized message
      throw new IllegalStateException("Order tanpa baris tidak bisa di-Book");
    }

    if (StringUtils.isBlank(order.getOrderNo())) {
      // hanya ambil no order kalau belum diset
      String year = SDF_YY.format(order.getOrderDate());
      String seqKey = String.format("ORDER_%s%02d", year, order.getBatch().getBatchNo());
      long number = uniqueNumbersAPI.getNextNumber(seqKey);
      String orderNo = String.format("%s%02d%03d", year, order.getBatch().getBatchNo(), number);
      order.setOrderNo(orderNo);
    }

    // update Order
    order.setBooked(true);
    transactionalDataManager.save(order);
    logger.debug("END: Book Order {}", order);
  }

  @Override
  public void unbookOrder(Order order) {
    logger.debug("START: Unbook Order {}", order);
    if (!Boolean.valueOf(order.getBooked())) {
      throw new IllegalStateException("Order belum di-Book");
    }

    order.setBooked(false);
    transactionalDataManager.save(order);
    logger.debug("END: Unbook Order {}", order);
  }

  public byte[] print(Set<Order> orders) throws AppException {
    List<Map<String, Object>> parametersList = new ArrayList<>();
    byte[] result;
    try {
      Connection con = persistence.getDataSource().getConnection();

      orders.stream().forEach(o -> {
            HashMap<String, Object> parameters = new HashMap<>();

            parameters.put("REPORT_CONNECTION", con);
            parameters.put("ORDER_ID", o.getId());
            parameters.put("TENANT_ID" , tenantProvider.getCurrentUserTenantId());
            parametersList.add(parameters);
          }
      );

      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      InputStream template = null;
      try {
        template = getClass().getResourceAsStream(
            "/id/co/danwinciptaniaga/jessys/report/Jessy_Invoice.jrxml");
        logger.info("Found invoice template: {}", template != null);
        JasperUtil.printReports(template, parametersList, JasperUtil.MODE_PDF, baos);
        baos.flush();
        result = baos.toByteArray();
      } finally {
        baos.close();
        if (template != null)
          template.close();
      }
    } catch (Exception e) {
      logger.error("Error printing invoice", e);
      throw new AppException("Error printing invoice", e);
    }
    return result;
  }
}