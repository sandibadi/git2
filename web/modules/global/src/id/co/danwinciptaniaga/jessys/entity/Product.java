package id.co.danwinciptaniaga.jessys.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.haulmont.addon.sdbmt.core.TenantId;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseUuidEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.TenantEntity;
import com.haulmont.cuba.core.entity.Updatable;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

@NamePattern("%s|name")
@Table(name = "JESSYS_PRODUCT")
@Entity(name = "jessys_Product")
public class Product extends BaseUuidEntity implements Updatable, Creatable, TenantEntity {
  private static final long serialVersionUID = -76571050118457304L;

  @Column(name = "UPDATE_TS")
  protected Date updateTs;

  @Column(name = "UPDATED_BY", length = 50)
  protected String updatedBy;

  @Column(name = "CREATE_TS")
  protected Date createTs;

  @Column(name = "CREATED_BY", length = 50)
  protected String createdBy;

  @NotNull
  @Column(name = "CODE", nullable = false, unique = true, length = 20)
  protected String code;

  @NotNull
  @Column(name = "NAME", nullable = false, unique = true)
  protected String name;

  @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
  @NotNull
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "SUPPLIER_ID")
  protected Supplier supplier;

  @NotNull
  @Column(name = "PURCHASE_PRICE", nullable = false, precision = 19, scale = 0)
  protected BigDecimal purchasePrice;

  @Column(name = "SELLING_PRICE", precision = 19, scale = 0)
  protected BigDecimal sellingPrice;

  @TenantId
  @Column(name = "TENANT_ID")
  protected String tenantId;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Supplier getSupplier() {
    return supplier;
  }

  public void setSupplier(Supplier supplier) {
    this.supplier = supplier;
  }

  public BigDecimal getSellingPrice() {
    return sellingPrice;
  }

  public void setSellingPrice(BigDecimal sellingPrice) {
    this.sellingPrice = sellingPrice;
  }

  public BigDecimal getPurchasePrice() {
    return purchasePrice;
  }

  public void setPurchasePrice(BigDecimal purchasePrice) {
    this.purchasePrice = purchasePrice;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getCreatedBy() {
    return createdBy;
  }

  @Override
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public Date getCreateTs() {
    return createTs;
  }

  @Override
  public void setCreateTs(Date createTs) {
    this.createTs = createTs;
  }

  @Override
  public String getUpdatedBy() {
    return updatedBy;
  }

  @Override
  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  @Override
  public Date getUpdateTs() {
    return updateTs;
  }

  @Override
  public void setUpdateTs(Date updateTs) {
    this.updateTs = updateTs;
  }

  public String getTenantId() {
    return tenantId;
  }

  public void setTenantId(String tenantId) {
    this.tenantId = tenantId;
  }
}