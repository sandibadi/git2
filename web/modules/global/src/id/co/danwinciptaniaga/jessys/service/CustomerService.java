package id.co.danwinciptaniaga.jessys.service;

public interface CustomerService {
  String NAME = "jessys_CustomerService";

  long getNextCustCode();
}