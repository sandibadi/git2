package id.co.danwinciptaniaga.jessys.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.haulmont.addon.sdbmt.core.TenantId;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseUuidEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.TenantEntity;
import com.haulmont.cuba.core.entity.Updatable;

@NamePattern("%s - %s|name,address")
@Table(name = "JESSYS_CUSTOMER")
@Entity(name = "jessys_Customer")
public class Customer extends BaseUuidEntity implements Creatable, Updatable, TenantEntity {
  private static final long serialVersionUID = 2074458353667582461L;

  @Column(name = "CREATE_TS")
  protected Date createTs;

  @Column(name = "CREATED_BY", length = 50)
  protected String createdBy;

  @Column(name = "UPDATE_TS")
  protected Date updateTs;

  @Column(name = "UPDATED_BY", length = 50)
  protected String updatedBy;

  @NotNull
  @Column(name = "CODE", nullable = false, unique = true)
  protected Long code;

  @Column(name = "NAME")
  protected String name;

  @NotNull
  @Column(name = "ADDRESS", nullable = false)
  protected String address;

  @Column(name = "PHONE", length = 15)
  protected String phone;

  @NotNull
  @Column(name = "ACTIVE", nullable = false)
  protected Boolean active = true;

  @TenantId
  @Column(name = "TENANT_ID")
  protected String tenantId;

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getCode() {
    return code;
  }

  public void setCode(Long code) {
    this.code = code;
  }

  @Override
  public String getUpdatedBy() {
    return updatedBy;
  }

  @Override
  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  @Override
  public Date getUpdateTs() {
    return updateTs;
  }

  @Override
  public void setUpdateTs(Date updateTs) {
    this.updateTs = updateTs;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Date getCreateTs() {
    return createTs;
  }

  public void setCreateTs(Date createTs) {
    this.createTs = createTs;
  }

  public String getTenantId() {
    return tenantId;
  }

  public void setTenantId(String tenantId) {
    this.tenantId = tenantId;
  }
}