package id.co.danwinciptaniaga.jessys.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.haulmont.addon.sdbmt.core.TenantId;
import com.haulmont.chile.core.annotations.Composition;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseUuidEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.TenantEntity;
import com.haulmont.cuba.core.entity.Updatable;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

@NamePattern("%s %s %s|orderNo,customerName,orderDate")
@Table(name = "JESSYS_ORDER")
@Entity(name = "jessys_Order")
public class Order extends BaseUuidEntity implements Updatable, Creatable, TenantEntity {
  private static final long serialVersionUID = -837691950743694635L;

  @Column(name = "UPDATE_TS")
  protected Date updateTs;

  @Column(name = "UPDATED_BY", length = 50)
  protected String updatedBy;

  @Column(name = "CREATE_TS")
  protected Date createTs;

  @Column(name = "CREATED_BY", length = 50)
  protected String createdBy;

  @NotNull
  @Lookup(type = LookupType.DROPDOWN, actions = {})
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "BATCH_ID")
  protected Batch batch;

  @Column(name = "ORDER_NO", unique = true, length = 30)
  protected String orderNo;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CUSTOMER_ID")
  protected Customer customer;

  @NotNull
  @Column(name = "CUSTOMER_NAME", nullable = false)
  protected String customerName;

  @Column(name = "DESCRIPTION", length = 2000)
  protected String description;

  @Column(name = "ADDRESS")
  protected String address;

  @Column(name = "PHONE", length = 15)
  protected String phone;

  @Column(name = "SALES_REP")
  protected String salesRep;

  @NotNull
  @Column(name = "DELIVERY_FEE", nullable = false, precision = 19, scale = 0)
  protected BigDecimal deliveryFee;

  @NotNull
  @Column(name = "SUBTOTAL", nullable = false, precision = 19, scale = 0)
  protected BigDecimal subtotal;

  @NotNull
  @Column(name = "GRANDTOTAL", nullable = false, precision = 19, scale = 0)
  protected BigDecimal grandtotal;

  @Temporal(TemporalType.DATE)
  @NotNull
  @Column(name = "ORDER_DATE", nullable = false)
  protected Date orderDate;

  @Temporal(TemporalType.DATE)
  @Column(name = "INVOICE_DATE")
  protected Date invoiceDate;

  @NotNull
  @Column(name = "INVOICED", nullable = false, columnDefinition = "boolean default false")
  protected Boolean invoiced = false;

  @NotNull
  @Column(name = "BOOKED", nullable = false, columnDefinition = "boolean default false")
  protected Boolean booked = false;

  @OrderBy("line")
  @Composition
  @OneToMany(mappedBy = "order")
  protected List<OrderLine> orderLines;

  @Column(name = "FULLY_PAID")
  protected Boolean fullyPaid = false;

  @TenantId
  @Column(name = "TENANT_ID")
  protected String tenantId;

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Boolean getFullyPaid() {
    return fullyPaid;
  }

  public void setFullyPaid(Boolean fullyPaid) {
    this.fullyPaid = fullyPaid;
  }

  public Batch getBatch() {
    return batch;
  }

  public void setBatch(Batch batch) {
    this.batch = batch;
  }

  public List<OrderLine> getOrderLines() {
    return orderLines;
  }

  public void setOrderLines(List<OrderLine> orderLines) {
    this.orderLines = orderLines;
  }

  public BigDecimal getGrandtotal() {
    return grandtotal;
  }

  public void setGrandtotal(BigDecimal grandtotal) {
    this.grandtotal = grandtotal;
  }

  public BigDecimal getSubtotal() {
    return subtotal;
  }

  public void setSubtotal(BigDecimal subtotal) {
    this.subtotal = subtotal;
  }

  public BigDecimal getDeliveryFee() {
    return deliveryFee;
  }

  public void setDeliveryFee(BigDecimal deliveryFee) {
    this.deliveryFee = deliveryFee;
  }

  public Boolean getBooked() {
    return booked;
  }

  public void setBooked(Boolean booked) {
    this.booked = booked;
  }

  public Boolean getInvoiced() {
    return invoiced;
  }

  public void setInvoiced(Boolean invoiced) {
    this.invoiced = invoiced;
  }

  public Date getInvoiceDate() {
    return invoiceDate;
  }

  public void setInvoiceDate(Date invoiceDate) {
    this.invoiceDate = invoiceDate;
  }

  public Date getOrderDate() {
    return orderDate;
  }

  public void setOrderDate(Date orderDate) {
    this.orderDate = orderDate;
  }

  public String getSalesRep() {
    return salesRep;
  }

  public void setSalesRep(String salesRep) {
    this.salesRep = salesRep;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getCustomerName() {
    return customerName;
  }

  public void setCustomerName(String customerName) {
    this.customerName = customerName;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  @Override
  public String getCreatedBy() {
    return createdBy;
  }

  @Override
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public Date getCreateTs() {
    return createTs;
  }

  @Override
  public void setCreateTs(Date createTs) {
    this.createTs = createTs;
  }

  @Override
  public String getUpdatedBy() {
    return updatedBy;
  }

  @Override
  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  @Override
  public Date getUpdateTs() {
    return updateTs;
  }

  @Override
  public void setUpdateTs(Date updateTs) {
    this.updateTs = updateTs;
  }

  public String getTenantId() {
    return tenantId;
  }

  public void setTenantId(String tenantId) {
    this.tenantId = tenantId;
  }
}